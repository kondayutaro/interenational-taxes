from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
import pyautogui
import datetime
from webdriver_manager.chrome import ChromeDriverManager

class WebDriver():
    def __init__(self):
        chrome_options = Options()
        chrome_options.add_experimental_option('prefs',  {
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
            "plugins.plugins_disabled": ["Chrome PDF Viewer"]
            }
        )
        chrome_options.add_argument('--kiosk-printing')

        self.driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)
        self.downloaded_pdfs = []

    def login(self, username, password):
        # Access SBI Securities
        self.driver.get("https://www.sbisec.co.jp/ETGate")
        # Get components
        WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.XPATH, '/html/body/table/tbody/tr[1]/td[2]/form/div/div/div/dl/dd[1]/div/input')))
        username_field = self.driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td[2]/form/div/div/div/dl/dd[1]/div/input')
        password_field = self.driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td[2]/form/div/div/div/dl/dd[2]/div/input')
        login_button = self.driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td[2]/form/div/div/div/p[2]/a/input')
        # Fill the blank
        username_field.send_keys(username)
        password_field.send_keys(password)
        login_button.click()

    def locate_mailbox(self):
        WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/div/ul/li[3]/a/img')))
        account_manage = self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/ul/li[3]/a/img')
        account_manage.click()
        WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[3]/div/div/ul/li[9]/div/a')))
        mailbox = self.driver.find_element_by_xpath('/html/body/div[1]/div[3]/div/div/ul/li[9]/div/a')
        mailbox.click()
        WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH, '/html/body/table/tbody/tr/td/table[2]/tbody/tr/td[2]/div/table[2]/tbody/tr[1]/td/table/tbody/tr[2]/td[4]/a/img')))
        browse_button = self.driver.find_element_by_xpath('/html/body/table/tbody/tr/td/table[2]/tbody/tr/td[2]/div/table[2]/tbody/tr[1]/td/table/tbody/tr[2]/td[4]/a/img')
        browse_button.click()

    def focus_on_new_window(self):
        WebDriverWait(self.driver, 10).until(EC.number_of_windows_to_be(2))
        new_window = self.driver.window_handles[1]
        self.driver.switch_to_window(new_window)

    def fill_search_conditions(self, search_word):
        # Wait until search word can be entered
        WebDriverWait(self.driver, 600).until(EC.presence_of_element_located((By.CLASS_NAME, 'textbox')))
        self.driver.execute_script("""
        document.getElementsByClassName('textbox')[0].value = '{0}';
        """.format(search_word))
        # Get search period
        start_month = self.calculate_period()[0] + self.calculate_period()[1]
        end_month = self.calculate_period()[2] + self.calculate_period()[3]

        start_month_field = self.driver.find_element_by_xpath('/html/body/form[1]/div[1]/main/article/div/div[1]/div[2]/div[2]/div/ul/li[1]/select')
        end_month_field = self.driver.find_element_by_xpath('/html/body/form[1]/div[1]/main/article/div/div[1]/div[2]/div[2]/div/ul/li[3]/select')

        # Get appropriate month options
        start_month_to_be_selected = Select(start_month_field)
        end_month_to_be_selected = Select(end_month_field)

        # Wait until start_month_field becomes selectable
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/form[1]/div[1]/main/article/div/div[1]/div[2]/div[2]/div/ul/li[1]/select')))
        start_month_to_be_selected.select_by_value(start_month)
        end_month_to_be_selected.select_by_value(end_month)

    def calculate_period(self) -> list[str]:
        current_date = self.get_month_and_year()
        if current_date[1] <= 2:
            return [str(current_date[0] - 1), '01', str(current_date[0] - 1), '12']
        else:
            month_str = ''
            if current_date[1] < 10:
                month_str = '0' + str(current_date[1])
            else:
                month_str = str(current_date[1])
            return [str(current_date[0]), '01', str(current_date[0]), month_str]

    def get_month_and_year(self) -> list[int]:
        current_date = datetime.datetime.today()
        # Get current year
        current_year = current_date.year
        # Get current month
        current_month = current_date.month
        return [current_year, current_month]

    def click_narrow_down_btn(self):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/form[1]/div[1]/main/article/div/div[1]/div[2]/div[2]/div/ul/div/li[2]/input')))
        self.driver.find_element_by_xpath('/html/body/form[1]/div[1]/main/article/div/div[1]/div[2]/div[2]/div/ul/div/li[2]/input').click()

    def get_item_number(self) -> str:
        return self.driver.find_elements_by_class_name('number')[0].text

    def get_pdf_locations(self):
        # Get the number of PDFs
        number = int(self.get_item_number())
        print("Start of PDF opening")
        for i in range(1, number+1):
            pdf_download_btn_path = f'/html/body/form[1]/div[1]/main/article/div/div[4]/ul[2]/li[{i}]/div[2]/div[5]/a'
            date_path = f'/html/body/form[1]/div[1]/main/article/div/div[4]/ul[2]/li[{i}]/div[2]/div[1]'
            WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.XPATH, pdf_download_btn_path)))
            # Get date for naming a PDF file
            date = self.driver.find_element_by_xpath(date_path).get_attribute('textContent').replace('/', '_')
            pdf_btn = self.driver.find_element_by_xpath(pdf_download_btn_path)
            pdf_btn.click()
            # Wait till the PDF page opens
            WebDriverWait(self.driver, 5).until(EC.number_of_windows_to_be(2+i))
            # Switch to the PDF page
            self.driver.switch_to_window(self.driver.window_handles[-1])
            self.driver.execute_script('window.print();')
            sleep(3)
            self.download_pdf(i, date)
        print("End of PDF opening")

    def download_pdf(self, pdf_index, date):
        # Name a PDF file
        pdf_name = f'tax_{pdf_index}_{date}.pdf'
        pyautogui.typewrite(pdf_name)
        pyautogui.press("enter")
        print(pdf_name)
        print(self.driver.current_url)
        self.downloaded_pdfs += pdf_name
        # Switch back to the mail inbox
        self.driver.switch_to_window(self.driver.window_handles[1])

    def close(self):
        self.driver.quit()
