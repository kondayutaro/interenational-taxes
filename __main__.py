from selenium import webdriver
from web_driver import WebDriver
from secrets import UserInfo

user = UserInfo()
web_driver = WebDriver()
web_driver.login(user.user_name, user.password)
print("Login successful")
web_driver.locate_mailbox()
web_driver.focus_on_new_window()
web_driver.fill_search_conditions('外国株式等配当金等のご案内（兼）支払通知書')
print("Search conditions filled")
web_driver.click_narrow_down_btn()
print("Mails narrowed down")
web_driver.get_pdf_locations()
web_driver.close
